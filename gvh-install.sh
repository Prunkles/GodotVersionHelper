#!/usr/bin/env bash

# Check for root
if [[ $EUID -ne 0 ]]; then
    echo "You need to run this script as root."
    exit 1
fi

GVH_DIR=$HOME/.gvh

mkdir -p ${GVH_DIR}
mkdir -p ${GVH_DIR}/versions

cp Godot_icon.svg ${GVH_DIR}/Godot_icon.svg
chmod 777 ${GVH_DIR}/Godot_icon.svg
echo "HERE_SHOULD_BE_YOUR_SELECTED_VERSION" > ${GVH_DIR}/selected
chmod 777 ${GVH_DIR}/selected

sudo cp godot.py /usr/local/bin/godot
sudo chmod +x /usr/local/bin/godot

# Finish message
echo GodotVersionHelper is installed!
echo Type \`godot\` to run and configure it.