How to use?
-----------

1. Run `sudo bash gvh-install.sh` to install `godot` file to `/usr/local/bin/` and init `~/.gvh/` folder.
2. Next download a version with `godot download --url URL`, where you can find URL here https://downloads.tuxfamily.org/godotengine/ (direct link).
3. Now run `godot select --list` to see your installed versions, and run `godot select -n N` to select the version by number.
4. And finally run `godot run` and enjoy using Godot \\^o^/
5. And also you can run `ģodot --desktop` to create a desktop icon to fast launch.