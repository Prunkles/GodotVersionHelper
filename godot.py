#!/usr/bin/python3.6
import os
import sys
import argparse
from pathlib import Path
import urllib.request
import zipfile
import subprocess

GVH_VERSION = "0.5"

gvh_path = Path.home() / ".gvh"
selected_path = gvh_path / "selected"
versions_path = gvh_path / "versions"


class Version:
    name = ""
    path = Path

    def __init__(self, name, path):
        self.name = name
        self.path = path


versions = list()
selected_v = Version(None, None)


# Run


def run(namespace):
    version_path = list(selected_v.path.glob("Godot_*.*"))[0]
    os.chdir(str(selected_v.path))

    # TODO Do this else way
    version_path.chmod(0o777)

    print("Run %s" % version_path.name)
    subprocess.run(str(version_path))


# Download


def download(namespace):
    if namespace.list:
        print("WIP")
        exit(6)
        return

    if namespace.number is not None:
        print("WIP")
        exit(6)
        return

    if namespace.url is not None:
        print("Downloading %s ..." % namespace.url)
        urllib.request.urlretrieve(namespace.url, "/tmp/gvh-download.zip")
        print("Unzipping...")
        with zipfile.ZipFile("/tmp/gvh-download.zip", "r") as zip_ref:
            zip_ref.extractall(str(versions_path))
        print("Done.")
        return


# Select


def select(namespace):
    if namespace.version is not None and namespace.number is not None:
        print("Only -n or -v can be used in the same time.")
        exit(5)

    if namespace.list:
        i = 1
        for v in versions:
            print("%i: %s" % (i, v.name))
            i += 1
        return

    if namespace.number is not None:
        if namespace.number <= 0 or namespace.number > len(versions):
            print("Version number is out of versions list.")
            exit(4)
        v = versions[namespace.number - 1]
        selected_path.write_text(v.name)
        print("Selected: %s" % v.name)
        return

    if namespace.version is not None:
        find = False
        for v in versions:
            if v.name == namespace.version:
                selected_path.write_text(str(v))
                find = True
        if not find:
            print("Can't find this version name.")
            exit(3)
        print("Selected: %s" % namespace.version)

        return


# Main


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--desktop", action="store_true",
                        help="Create desktop icon which will run selected version.")

    subparsers = parser.add_subparsers(dest="mode")

    run_parser = subparsers.add_parser("run")
    run_parser.add_argument("-e", "--external", action="store_true",
                            help="Use external command-line interface")

    download_parser = subparsers.add_parser("download")
    download_parser.add_argument("-l", "--list", action="store_true",
                                 help="Show the list of accessible versions.")
    download_parser.add_argument("-n", "--number", type=int,
                                 help="Select from --list number.")
    download_parser.add_argument("-u", "--url", type=str,
                                 help="Direct link to ZIP.")

    select_parser = subparsers.add_parser("select")
    select_parser.add_argument("-l", "--list", action="store_true",
                               help="Show the list of accessible versions.")
    select_parser.add_argument("-n", "--number", type=int,
                               help="Select from --list number.")
    select_parser.add_argument("-v", "--version", type=str,
                               help="Select from version name.")

    return parser


def init():
    # Load versions
    for v_path in versions_path.iterdir():
        versions.append(Version(v_path.name, v_path))

    # Load selected
    global selected_v
    v_name = selected_path.read_text()
    for v in versions:
        if v.name == v_name:
            selected_v = v
            break


def main(argv):
    try:
        init()
    except all:
        print("Failed to load versions list.")
        exit(1)

    parser = create_parser()

    namespace = parser.parse_args()

    if namespace.mode == "run":
        run(namespace)
    elif namespace.mode == "select":
        select(namespace)
    elif namespace.mode == "download":
        download(namespace)

    if namespace.desktop:
        desktop_path = Path.home() / "Desktop/Godot.desktop"
        desktop_path.write_text(
            """
[Desktop Entry]
Version=%s
Name=Godot
Comment=Game Engine
Exec=godot run
Terminal=false
Icon=%s/.gvh/Godot_icon.svg
Type=Application
Categories=Development;
""" % (GVH_VERSION, str(Path.home())))
        print("Desktop shortcut has been created. Enjoy!")


if __name__ == '__main__':
    main(sys.argv)
